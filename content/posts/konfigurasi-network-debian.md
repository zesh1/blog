---
title: "Konfigurasi IP Address static di Debian 11"
date: 2021-12-04T12:23:37+07:00
draft: true
toc: false
images:
tags:
  - untagged
---

Langkah - langkah konfigurasi network interface di Debian 11


1. Masuk konfigurasi file yang berada di /etc/network/interfaces
{{< highlight bash >}}
vim /etc/network/interfaces
{{< /highlight>}}

2. Tambahkan beberapa konfigurasi seperti dibawah ini, sesuaikan dengan nama ethernet yang ada
{{< highlight bash >}}
auto enp1s0
iface enp1s0 inet static
	address 192.168.16.10
	netmask 255.255.255.0
	network 192.168.16.0
	broadcast 192.168.16.255
	gateway 192.168.16.1
{{< /highlight>}}
















