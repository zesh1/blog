---
title: "Instalasi Ssh Server di Debian"
date: 2021-11-24T20:36:55+07:00
draft: false
toc: false
images:
tags:
  - untagged
---

#### Langkah Langkah Instalasi SSH (Secure Shell) di Debian

**1. Update repository debian**  
{{< highlight bash >}}
apt update
apt ugrade	#opsional
{{< /highlight >}}

**2. Install package openssh-server**  
{{< highlight bash >}}
apt install openssh-server
{{< /highlight >}}

**3. Start ssh service**  
{{< highlight go >}}
systemctl start ssh 	// start ssh
systemctl restart ssh	// restart ssh
systemctl enable ssh	// autostart ssh saat booting
{{< /highlight >}}
Default port dari ssh adalah port 22  
Kita bisa langsung menggunakan ssh tanpa mengganti port  

- **Test remote dari pc client "linux" dengan default port**  
Perintah yang digunakan:  
ssh (userserver)@(ipserver)  
{{< highlight go >}}
ssh bullseye@192.168.1.50
{{< /highlight >}}

**3. Mengganti port ssh**  
Jika tidak ingin menggunakan default port dan ingin menggatinya, kita bisa merubah portnya di file konfigurasi yang berada di /etc/ssh/sshd_config  

{{< highlight go >}}
vim /etc/ssh/sshd_config
{{< /highlight >}}

Uncomment "#Port 22" dan ubah angka portnya, misalnya kita ganti menjadi Port 49190
{{< highlight bash >}}
 13 Include /etc/ssh/sshd_config.d/*.conf
 14 
 15 Port 49190
 16 #AddressFamily any
 17 #ListenAddress 0.0.0.0
{{< /highlight >}}

Kemudian restart ssh dengan perintah:  
{{< highlight bash >}}
systemctl restart ssh
{{< /highlight >}}

Selanjunya kita cek portnya terganti atau belum menggunakan perintah:
{{< highlight bash >}}
netstat -tulpn
{{< /highlight >}}

{{< highlight bash >}}
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:49190           0.0.0.0:*               LISTEN      414/sshd: /usr/sbin 
tcp        0      0 0.0.0.0:21              0.0.0.0:*               LISTEN      416/proftpd: (accep 
tcp6       0      0 :::49190                :::*                    LISTEN      414/sshd: /usr/sbin 
{{< /highlight >}}

Jika port ssh sudah terganti maka konfigurasi port ssh sudah berhasil

- **Tes remote dari pc client "linux" dengan custom port**  
Perintah yang digunakan:  
ssh (userserver)@(ipserver) -p (port yang kita ubah)  
{{< highlight bash >}}
ssh bullseye@192.168.1.50 -p 49190
{{< /highlight >}}

