---
title: "Cara install Dwm di Debian 11/Debian Based Distro"
date: 2021-11-22T17:31:10+07:00
draft: false
toc: false
images:
tags:
  - untagged


markup:
  highlight:
    anchorLineNos: false
    codeFences: true
    guessSyntax: true
    hl_Lines: ""
    lineAnchors: ""
    lineNoStart: 1
    lineNos: false
    lineNumbersInTable: true
    noClasses: true
    style: monokai
    tabWidth: 4


---


**1. Update Repository**  

{{< highlight bash >}}
~$ sudo apt update && sudo apt upgrade  
{{< /highlight >}}

**2. Install package** 
 
- install beberapa software yang dibutuhkan :
{{< highlight bash >}}
~$ sudo apt install git suckless-tools libxft-dev libx11-dev libxinerama-dev build-essential
{{< /highlight >}}

- Debian minimal install
{{< highlight bash >}}
~$ sudo apt install libxft-dev libfontconfig1 libx11-6 libxinerama-dev suckless-tools xorg x11-xserver-utils stterm xserver-xorg-dev
{{< /highlight >}}

**3. Install dwm, dmenu, st**  
- **install dwm**, window manager yang akan kita gunakan  
  clone dwm dari git repository suckless.org
{{< highlight bash >}}
~$ mkdir suckless  
~$ cd suckless  
~/suckless$ git clone https://git.suckless.org/dwm.git
~/suckless$ ls
dwm
~/suckless$
{{< /highlight >}}

  compile dwm
{{< highlight bash >}}
~/suckless$ cd dwm
~/suckless/dwm$ sudo make install
{{< /highlight >}}

- **install dmenu**, software menu yang akan kita gunakan
  clone dmenu dari git repository suckless.org
{{< highlight bash >}}
~/suckless/dwm$ cd ..
~/suckless$ git clone https://git.suckless.org/dmenu
~/suckless$ ls
dmenu  dwm
~/suckless/dwm$
{{< /highlight >}}

  compile dan install dmenu
{{< highlight bash >}}
~/suckless$ ls
dmenu  dwm
~/suckless$ cd dmenu
~/suckless/dmenu$ sudo make install
{{< /highlight >}}

- **instal st**, terminal yang akan kita install sebagai terminal emulator yang akan kita pakai
  clone st dari git repository suckless.org
{{< highlight bash >}}
~/suckless$ git clone https://git.suckless.org/st
~/suckless$ ls
dmenu  dwm  st
{{< /highlight >}}

  compile dan install st
{{< highlight bash >}}
~/suckless$ cd st
~/suckless/st$ sudo make install
{{< /highlight >}}

Setelah dwm, st, dan dmenu telah kita install, selanjutnya kita tambahkan dwm ke session display manager.[cara menambahkan dwm ke lightdm, display manager]() _(coming soon)_  
  
